import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'login.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chrono Scheduler',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: TampilanLogin(),
    );
  }
}