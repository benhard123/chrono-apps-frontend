import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

import 'home.dart';

Future<Login> createLogin(String username, String password) async {
  final http.Response response = await http.post(
    'http://ec2-18-225-9-119.us-east-2.compute.amazonaws.com:8000/login/',
//    'http://192.168.18.105:8000/login/',
    headers: <String, String>{
      'Content-Type' : 'application/json; charset=UTF-8'
    },
    body: jsonEncode(<String, String>{
      'username' : username,
      'password' : password,
    }),
  );

  if (response.statusCode == 201){
    return Login.fromJson(json.decode(response.body), response.statusCode);
  } else if(response.statusCode == 400){
    Map <String, dynamic> errordata = json.decode(response.body);
    throw Exception(errordata);
  }
  else{
    throw Exception('Failed to create login.');
  }
}

class Login{
  final Map<String, dynamic> data;
  final int statusCode;

  Login({this.data, this.statusCode});

  factory Login.fromJson(Map<String, dynamic> json, int status){
    return Login(
      data: json,
      statusCode: status,
    );
  }
}

class TampilanLogin extends StatefulWidget{
  @override
  TampilanLoginState createState() => TampilanLoginState();
}

class TampilanLoginState extends State<TampilanLogin>{
  final TextEditingController _controller1 = TextEditingController();
  final TextEditingController _controller2 = TextEditingController();
  Future<Login> _futureLogin;

  Widget _loginstatus(){
    if(_futureLogin!=null){
      return FutureBuilder<Login>(
          future: _futureLogin,
          builder: (context, snapshot){
            if(snapshot.hasData){
              if(snapshot.data.statusCode == 201){
                return SizedBox(width: 5, height: 1,);
              }
            }
            else if(snapshot.hasError){
              return SizedBox(width: 5, height: 1,);
            }
            return CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            );
          });
    }
    else{
      return SizedBox(width: 5, height: 30,);
    }
  }

  void _pressedLogin(){
    setState(() {
      _futureLogin = createLogin(_controller1.text, _controller2.text)
          .then((value) {
        Navigator.of(context).pushReplacement(HomeAppsRoute(
          data: value.data));
        return value;})
          .catchError((value){
        return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: Text(''+value.toString()),
            actions: <Widget>[
              new GestureDetector(
                onTap: () => Navigator.of(context).pop(),
                child: Text("ok"),
              ),
            ],
          ),
        );
      });
    });
  }

  @override
  Widget build(BuildContext context){;
  return Scaffold(
    backgroundColor: Color.fromARGB(255, 64, 140, 255),
//    appBar: AppBar(
//      title: Text('Login'),
//    ),
    body: Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.all(15.0),
      child:  ListView(
          children: <Widget>[
            Image.asset('image/ChronoApp Logo.png'),
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Login",
                style: TextStyle(
                  fontSize: 30,
                  color: Color.fromARGB(255, 246, 255, 4),
                ),
              ),
            ),
            SizedBox(width: 5, height: 10,),
            TextField(
              controller: _controller1,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white,
                hintText: 'Email',
                contentPadding: EdgeInsets.all(8.0),
                border: const OutlineInputBorder(),
              ),
            ),
            SizedBox(width: 5, height: 30,),
            TextField(
              obscureText: true,
              controller: _controller2,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white,
                hintText: 'Password',
                contentPadding: EdgeInsets.all(8.0),
                border: const OutlineInputBorder(),
              ),
            ),
            SizedBox(width: 5, height: 30,),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                _loginstatus(),
                SizedBox(width: 20, height: 1,),
                RaisedButton(
                  onPressed: _pressedLogin,
                  child: Text(
                    'Login',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ],
            ),
          ],
      ),
    ),
  );
  }
}