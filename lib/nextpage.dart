import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:calendar_strip/calendar_strip.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:http/http.dart' as http;

Future<Jadwal> createListJadwal(int id) async {
  final http.Response response = await http.post(
    'http://ec2-18-225-9-119.us-east-2.compute.amazonaws.com:8000/kegiatan/',
//    'http://192.168.18.105:8000/kegiatan/',
    headers: <String, String>{
      'Content-Type' : 'application/json; charset=UTF-8'
    },
    body: jsonEncode(<String, int>{
      'id' : id,
    }),
  );

  if (response.statusCode == 200){
    return Jadwal.fromJson(json.decode(response.body), response.statusCode);
  }
  else{
    throw Exception('Failed to receive jadwal.');
  }
}

Future<http.Response> createJadwal({int id,String namakegiatan,String lokasikegiatan, String note, String start, String finish, String date}) async {
  final http.Response response = await http.post(
    'http://ec2-18-225-9-119.us-east-2.compute.amazonaws.com:8000/buatkegiatan/',
//    'http://192.168.18.105:8000/buatkegiatan/',
    headers: <String, String>{
      'Content-Type' : 'application/json; charset=UTF-8'
    },
    body: jsonEncode(<String, dynamic>{
      'name' : namakegiatan,
      'location' : lokasikegiatan,
      'note' : note,
      'start' : start,
      'finish' : finish,
      'accountid' : id,
      'date': date,
    }),
  );

  if (response.statusCode == 200){
    return response;
  }
  else{
    throw Exception('Failed to receive jadwal.');
  }
}

class Jadwal{
  final List<dynamic> data;
  final int statusCode;

  Jadwal({this.data, this.statusCode});

  factory Jadwal.fromJson(List<dynamic> json, int status){
    return Jadwal(
      data: json,
      statusCode: status,
    );
  }
}


class NextPage extends StatefulWidget{
  final Map<String, dynamic> data;
  final int index;
  NextPage({Key key,this.data, this.index}) : super(key: key);
  @override
  NextPageState createState() => NextPageState(data: data, index: index);
}

class NextPageState extends State<NextPage>{
  final Map<String, dynamic> data;
  int index;
  Future<Jadwal> _listjadwal;
  NextPageState({this.data, this.index}) : super();

  DateTime startDate = DateTime.now().subtract(Duration(days: 2));
  DateTime endDate = DateTime.now().add(Duration(days: 10));
  DateTime selectedDate = DateTime.now().subtract(Duration(days: 2));
  List<DateTime> markedDates = [
//    DateTime.now().subtract(Duration(days: 1)),
//    DateTime.now().subtract(Duration(days: 2)),
//    DateTime.now().add(Duration(days: 4))
  ];


  onselect(data){
    setState(() {
      selectedDate = data;
      _listjadwal = createListJadwal(this.data["id"]);
    });
  }

  _monthNameWidget(monthName) {
    return Container(
      child: Text(monthName,
          style: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w600,
              color: Colors.black87,
              fontStyle: FontStyle.italic
          )
      ),
      padding: EdgeInsets.only(top: 8, bottom: 4),
    );
  }

  getMarkedIndicatorWidget() {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(left: 1, right: 1),
            width: 7,
            height: 7,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.red
            ),
          ),
          Container(
            width: 7,
            height: 7,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.blue
            ),
          )
        ]
    );
  }

  dateTileBuilder(date, selectedDate, rowIndex, dayName, isDateMarked, isDateOutOfRange) {
    bool isSelectedDate = date.compareTo(selectedDate) == 0;
    Color fontColor = isDateOutOfRange ? Colors.black26 : Colors.black87;
    TextStyle normalStyle = TextStyle(fontSize: 17, fontWeight: FontWeight.w800, color: fontColor);
    TextStyle selectedStyle = TextStyle(fontSize: 17, fontWeight: FontWeight.w800, color: Colors.black87);
    TextStyle dayNameStyle = TextStyle(fontSize: 14.5, color: fontColor);
    List<Widget> _children = [
      Text(dayName, style: dayNameStyle),
      Text(date.day.toString(), style: !isSelectedDate ? normalStyle : selectedStyle),
    ];

    if (isDateMarked == true) {
      _children.add(getMarkedIndicatorWidget());
    }

    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      alignment: Alignment.center,
      padding: EdgeInsets.only(top: 8, left: 5, right: 5, bottom: 5),
      decoration: BoxDecoration(
        color: !isSelectedDate ? Colors.transparent : Colors.white70,
        borderRadius: BorderRadius.all(Radius.circular(60)),
      ),
      child: Column(
        children: _children,
      ),
    );
  }

  Widget _jadwalstatus(){
    if(_listjadwal!=null){
      return FutureBuilder<Jadwal>(
          future: _listjadwal,
          builder: (context, snapshot){
            if(snapshot.hasData){
              if(snapshot.data.statusCode == 200){
//                return Text(''+snapshot.data.data[0]["date"]);
                return ListView.builder(
                    itemCount: snapshot.data.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      String month = selectedDate.month.toString();
                      String day = selectedDate.day.toString();
                      if(selectedDate.month<10){
                        month = "0"+selectedDate.month.toString();
                      }
                      if(selectedDate.day<10){
                        day = "0"+selectedDate.day.toString();
                      }
                      if(snapshot.data.data[index]["date"]=="${selectedDate.year}-$month-$day")
                        return FlatButton(
                          color: Color.fromARGB(255, 64, 140, 255),
                          child: Container(
                            padding: EdgeInsets.only(left: 0, right: 0),
                            child: Column(children: <Widget>[
//                              Text(''+snapshot.data.data[index].toString()),
                              Row( children: <Widget>[
                                Text(snapshot.data.data[index]["start"].substring(0,5)+"-"+snapshot.data.data[index]["finish"].substring(0,5),
                                style: TextStyle(fontSize: 20),),
                                SizedBox(width: 10,height: 5,),
                                Column(children: <Widget>[
                                    Text(snapshot.data.data[index]["name"],
                                      style: TextStyle(fontSize: 25),),
                                    SizedBox(width: 10,height: 5,),
                                    Text(snapshot.data.data[index]["location"],
                                    style: TextStyle(fontSize: 22),)
                                ],)
                              ],),
                            ],),
                          ),
                          onPressed: (){
                            final TextEditingController _controlnama= TextEditingController(text: snapshot.data.data[index]["name"]);
                            final TextEditingController _controllokasi = TextEditingController(text: snapshot.data.data[index]["location"]);
                            final TextEditingController _controlnote = TextEditingController(text: snapshot.data.data[index]["note"]);
                            final TextEditingController _controlmulai = TextEditingController(text: snapshot.data.data[index]["start"].substring(0,5));
                            final TextEditingController _controlsesai = TextEditingController(text: snapshot.data.data[index]["finish"].substring(0,5));
                            final startformat = DateFormat("HH:mm");
                            final finishformat = DateFormat("HH:mm");
                            return showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                backgroundColor: Color.fromARGB(255, 208, 208, 208),
                                content: ListView(
                                  children: <Widget>[
                                    Text("Meeting name: "),
                                    SizedBox(width: 5, height: 10,),
                                    TextField(
                                      controller: _controlnama,
                                      decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
                                        contentPadding: EdgeInsets.all(8.0),
                                        border: const OutlineInputBorder(),
                                      ),
                                    ),
                                    SizedBox(width: 5, height: 10,),
                                    Text("Location: "),
                                    SizedBox(width: 5, height: 10,),
                                    TextField(
                                      controller: _controllokasi,
                                      decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
                                        contentPadding: EdgeInsets.all(8.0),
                                        border: const OutlineInputBorder(),
                                      ),
                                    ),
                                    SizedBox(width: 5, height: 10,),
                                    Row(
                                      children: <Widget>[
                                        Text("From: "),
                                        SizedBox(width: 80, height: 10,),
                                        Text("To: "),
                                      ],
                                    ),
                                    SizedBox(width: 5, height: 10,),
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          width: 100,
                                          child:DateTimeField(
                                            controller: _controlmulai,
                                            decoration: InputDecoration(
                                              filled: true,
                                              fillColor: Colors.white,
                                              contentPadding: EdgeInsets.all(3.0),
                                              border: const OutlineInputBorder(),
                                            ),
                                            format: startformat,
                                            onShowPicker: (context, currentValue) async {
                                              final time = await showTimePicker(
                                                context: context,
                                                initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                                              );
                                              if(time == null){
                                                return null;
                                              }
                                              return DateTimeField.convert(time);
                                            },
                                          ),
                                        ),
                                        SizedBox(width: 20, height: 10,),
                                        Container(
                                          width: 100,
                                          child:DateTimeField(
                                            controller: _controlsesai,
                                            decoration: InputDecoration(
                                              filled: true,
                                              fillColor: Colors.white,
                                              contentPadding: EdgeInsets.all(3.0),
                                              border: const OutlineInputBorder(),
                                            ),
                                            format: finishformat,
                                            onShowPicker: (context, currentValue) async {
                                              final time = await showTimePicker(
                                                context: context,
                                                initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                                              );
                                              if(time == null){
                                                return null;
                                              }
                                              return DateTimeField.convert(time);
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(width: 20, height: 10,),
                                    Text("Note: "),
                                    SizedBox(width: 5, height: 10,),
                                    TextField(
                                      controller: _controlnote,
                                      decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
                                        contentPadding: EdgeInsets.all(8.0),
                                        border: const OutlineInputBorder(),
                                      ),
                                    ),
                                  ],
                                ),
                                actions: <Widget>[
                                  RaisedButton(
                                    onPressed: (){
//                                      String month = selectedDate.month.toString();
//                                      String day = selectedDate.month.toString();
//                                      if(selectedDate.month<10){
//                                        month = "0" + selectedDate.month.toString();
//                                      }
//                                      if(selectedDate.day<10){
//                                        day = "0" + selectedDate.day.toString();
//                                      }
//                                      createJadwal(
//                                          id:data["id"],
//                                          namakegiatan: _controlnama.text,
//                                          note: _controlnote.text,
//                                          lokasikegiatan: _controllokasi.text,
//                                          start: _controlmulai.text+":00",
//                                          finish: _controlsesai.text+":00",
//                                          date:selectedDate.year.toString()+"-"+month+"-"+day
//                                      ).then((value) {
//                                        if(value.statusCode==200){
//                                          return showDialog(
//                                              context: context,
//                                              builder: (context) => AlertDialog(
//                                                content: Text('Data sudah disimpan'),
//                                                actions: <Widget>[
//                                                  new GestureDetector(
//                                                    onTap: () => Navigator.of(context).pop(),
//                                                    child: Text("ok"),
//                                                  ),
//                                                ],
//                                              )
//                                          ).then((value) {
//                                            Navigator.of(context).pop();
//                                          });
//                                        }
//                                        return null;
//                                      }
//                                      ).catchError((value){
//                                        return showDialog(
//                                            context: context,
//                                            builder: (context) => AlertDialog(
//                                              content: Text('ERROR'),
//                                              actions: <Widget>[
//                                                new GestureDetector(
//                                                  onTap: () => Navigator.of(context).pop(),
//                                                  child: Text("ok"),
//                                                ),
//                                              ],
//                                            )
//                                        ).then((value) {
//                                          Navigator.of(context).pop();
//                                        });
//                                      });
//                                      _listjadwal = createListJadwal(this.data["id"]);
                                    },
                                    child: Text(
                                      'Update',
//                      style: TextStyle(fontSize: 20),
                                    ),
                                  ),
                                  SizedBox(width: 120, height: 10,),
                                  RaisedButton(
                                    onPressed: () => Navigator.of(context).pop(),
                                    child: Text(
                                      'Cancel',
//                      style: TextStyle(fontSize: 20),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      else
                        return SizedBox(width: 0,height: 0,);
                    }
                );
              }
            }
            else if(snapshot.hasError){
              return Text(''+snapshot.error.toString());
            }
            return CircularProgressIndicator(

            );
          });
    }
    else{
      return SizedBox(width: 5, height: 30,);
    }
  }

  Widget Settings(){

  }

  Widget fromindex(int selectedindex){
    switch(selectedindex){
      case 0:
        return Container(
          child: Column(
            children: <Widget>[
              CalendarStrip(
                startDate: startDate,
                endDate: endDate,
                onDateSelected: onselect,
                dateTileBuilder: dateTileBuilder,
                iconColor: Colors.black87,
                monthNameWidget: _monthNameWidget,
                markedDates: markedDates,
                containerDecoration: BoxDecoration(color: Colors.yellow),
              ),
              Text("Selected Date -> "+selectedDate.day.toString()),
              Flexible(
                child:_jadwalstatus(),
              ),
            ],
          ),
        );
      case 1:
        return Text("Comming Soon");
      case 3:
        return Text("Comming Soon");
      case 4:
        return Container(
          padding: EdgeInsets.all(20),
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          alignment: Alignment.center,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage('https://via.placeholder.com/100'),
                        )
                    ),
                  ),
                  SizedBox(width: 20, height: 0,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text('Welcome,\n'+data["name"], style: TextStyle(fontSize: 20.0),),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 30, width: 10),
              Flexible(
                child: ListView(
                  children: <Widget>[
                    Container(
                      height: 50,
                      color: Colors.amber[600],
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child:Text('Entry A'),
                      ),
                    ),
                    Container(
                      height: 50,
                      color: Colors.amber[500],
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child:Text('Entry B'),
                      ),
                    ),
                    Container(
                      height: 50,
                      color: Colors.amber[100],
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child:Text('Entry C'),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        );
    }
  }
  
  fromindex2(int selectedindex){
    switch(selectedindex) {
      case 0:
        return FloatingActionButton(
          onPressed: () {
            final TextEditingController _controlnama= TextEditingController();
            final TextEditingController _controllokasi = TextEditingController();
            final TextEditingController _controlnote = TextEditingController();
            final TextEditingController _controlmulai = TextEditingController();
            final TextEditingController _controlsesai = TextEditingController();
            final startformat = DateFormat("HH:mm");
            final finishformat = DateFormat("HH:mm");
            return showDialog(
              context: context,
              builder: (context) => AlertDialog(
                backgroundColor: Color.fromARGB(255, 208, 208, 208),
                content: ListView(
                    children: <Widget>[
                      Text("Meeting name: "),
                      SizedBox(width: 5, height: 10,),
                      TextField(
                        controller: _controlnama,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          contentPadding: EdgeInsets.all(8.0),
                          border: const OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(width: 5, height: 10,),
                      Text("Location: "),
                      SizedBox(width: 5, height: 10,),
                      TextField(
                        controller: _controllokasi,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          contentPadding: EdgeInsets.all(8.0),
                          border: const OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(width: 5, height: 10,),
                      Row(
                        children: <Widget>[
                          Text("From: "),
                          SizedBox(width: 80, height: 10,),
                          Text("To: "),
                        ],
                      ),
                      SizedBox(width: 5, height: 10,),
                      Row(
                        children: <Widget>[
                          Container(
                            width: 100,
                            child:DateTimeField(
                              controller: _controlmulai,
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                contentPadding: EdgeInsets.all(3.0),
                                border: const OutlineInputBorder(),
                              ),
                              format: startformat,
                              onShowPicker: (context, currentValue) async {
                                final time = await showTimePicker(
                                  context: context,
                                  initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                                );
                                if(time == null){
                                  return null;
                                }
                                return DateTimeField.convert(time);
                              },
                            ),
                          ),
                          SizedBox(width: 20, height: 10,),
                          Container(
                            width: 100,
                            child:DateTimeField(
                              controller: _controlsesai,
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                contentPadding: EdgeInsets.all(3.0),
                                border: const OutlineInputBorder(),
                              ),
                              format: finishformat,
                              onShowPicker: (context, currentValue) async {
                                final time = await showTimePicker(
                                  context: context,
                                  initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                                );
                                if(time == null){
                                  return null;
                                }
                                return DateTimeField.convert(time);
                              },
                            ),
                          ),
                        ],
                      ),
                      SizedBox(width: 20, height: 10,),
                      Text("Note: "),
                      SizedBox(width: 5, height: 10,),
                      TextField(
                        controller: _controlnote,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          contentPadding: EdgeInsets.all(8.0),
                          border: const OutlineInputBorder(),
                        ),
                      ),
                    ],
                ),
                actions: <Widget>[
                  RaisedButton(
                    onPressed: (){
                      String month = selectedDate.month.toString();
                      String day = selectedDate.month.toString();
                      if(selectedDate.month<10){
                        month = "0" + selectedDate.month.toString();
                      }
                      if(selectedDate.day<10){
                        day = "0" + selectedDate.day.toString();
                      }
                      createJadwal(
                          id:data["id"],
                          namakegiatan: _controlnama.text,
                          note: _controlnote.text,
                          lokasikegiatan: _controllokasi.text,
                          start: _controlmulai.text+":00",
                          finish: _controlsesai.text+":00",
                          date:selectedDate.year.toString()+"-"+month+"-"+day
                      ).then((value) {
                        if(value.statusCode==200){
                          return showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              content: Text('Data sudah disimpan'),
                              actions: <Widget>[
                                new GestureDetector(
                                  onTap: () => Navigator.of(context).pop(),
                                  child: Text("ok"),
                                ),
                              ],
                            )
                          ).then((value) {
                            Navigator.of(context).pop();
                          });
                        }
                        return null;
                      }
                      ).catchError((value){
                          return showDialog(
                            context: context,
                              builder: (context) => AlertDialog(
                                  content: Text('ERROR'),
                                  actions: <Widget>[
                                      new GestureDetector(
                                        onTap: () => Navigator.of(context).pop(),
                                        child: Text("ok"),
                                      ),
                                    ],
                                )
                          ).then((value) {
                            Navigator.of(context).pop();
                          });
                      });
                      _listjadwal = createListJadwal(this.data["id"]);
                    },
                    child: Text(
                      'Save',
//                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  SizedBox(width: 120, height: 10,),
                  RaisedButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: Text(
                      'Discard',
//                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ],
              ),
            );
          },
          child: Icon(Icons.add),
          backgroundColor: Colors.green,
        );
      default:
        return null;
    }
  }

  @override
  Widget build(BuildContext context){
    return WillPopScope(
      child: Scaffold(
//       return Scaffold(
        backgroundColor: Color.fromARGB(255 , 255, 255, 255),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(125.0),
          child: AppBar(
//              automaticallyImplyLeading: false,
            title: Text('Chrono Scheduler'),
            flexibleSpace: Container(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                  children: <Widget>[
                    SizedBox(width: 5, height: 70,),
                    TextField(
                      style: TextStyle(fontSize: 15.0,),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(8.0),
                        filled: true,
                        fillColor: Colors.white,
                        border: const OutlineInputBorder(),
                      ),
                    ),
                  ]
              ),
            ),
            backgroundColor: Color.fromARGB(255, 64, 140, 255),
          ),
        ),
        body: fromindex(index),
        floatingActionButton: fromindex2(index),
        bottomNavigationBar: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Colors.green,
          ),
            child:BottomAppBar(
              color: Colors.green,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                IconButton(
                  iconSize: 50,
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  icon: Container(
                    width: 50,
                    height: 50,
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.purple,
                    ),
                    child: Icon(Icons.calendar_today, size:40,)
                  ),
                  onPressed: () {
                    setState(() {
                      index = 0;
                    });
                  },
                ),
                IconButton(
                  iconSize: 50,
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  icon: Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.purple,
                    ),
                    child: Icon(Icons.note, size:40,),
//                    Container(
//                      width: 50,
//                      height: 50,
//                      decoration: BoxDecoration(
//                          color: Colors.green,
//                          shape: BoxShape.rectangle,
//                          image: DecorationImage(
//                            fit: BoxFit.cover,
//                            image: NetworkImage('https://via.placeholder.com/50'),
//                          )
//                      ),
//                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      index = 1;
                    });
                  },
                ),
                  SizedBox(
                    width: 100.0,
                    height: 50.0,
                    child: FlatButton(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      child: Container(
                          width: 100,
                          height: 50,
//                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.purple,
                          ),
                          child: Icon(Icons.home, size:40,)
                      ),
                      onPressed: () {
                        setState(() {
                          Navigator.of(context).pop(true);
                        });
                      },
                    ),
                  ),
                IconButton(
                  iconSize: 50,
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  icon: Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.purple,
                    ),
                    child: Icon(Icons.notifications_active, size:40,),
//                    Container(
//                      width: 50,
//                      height: 50,
//                      decoration: BoxDecoration(
//                          color: Colors.green,
//                          shape: BoxShape.rectangle,
//                          image: DecorationImage(
//                            fit: BoxFit.cover,
//                            image: NetworkImage('https://via.placeholder.com/50'),
//                          )
//                      ),
//                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      index = 3;
                    });
                  },
                ),
                IconButton(
                  iconSize: 50,
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  icon: Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.purple,
                    ),
                    child: Icon(Icons.settings, size:40,),
//                    Container(
//                      width: 50,
//                      height: 50,
//                      decoration: BoxDecoration(
//                          color: Colors.green,
//                          shape: BoxShape.rectangle,
//                          image: DecorationImage(
//                            fit: BoxFit.cover,
//                            image: NetworkImage('https://via.placeholder.com/50'),
//                          )
//                      ),
//                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      index = 4;
                    });
                  },
                ),
              ],),
            ),
//          child: BottomNavigationBar(
//            type: BottomNavigationBarType.fixed,
//            items: <BottomNavigationBarItem>[
//              BottomNavigationBarItem(
//                  icon: Container(
//                    width: 50,
//                    height: 50,
//                    decoration: BoxDecoration(
//                        color: Color.fromARGB(255, 64, 140, 255),
//                        shape: BoxShape.rectangle,
//                    ),
//                    child: Container(
//                      width: 25,
//                      height: 25,
//                      decoration: BoxDecoration(
//                          shape: BoxShape.circle,
//                          image: DecorationImage(
//                            fit: BoxFit.cover,
//                            image: NetworkImage('https://via.placeholder.com/50'),
//                          ),
//                      ),
//                    ),
//                  ),
////                  Image.asset('image/50.png'),
//                  title: Text(''),
//                  backgroundColor: Colors.purple,
//              ),
//              BottomNavigationBarItem(
//                  icon: Icon(Icons.textsms),
//                  title: Text('Note'),
//              ),
//              BottomNavigationBarItem(
//                  icon: Icon(Icons.home),
//                  title: Text('Home'),
//              ),
//              BottomNavigationBarItem(
//                  icon: Icon(Icons.calendar_today),
//                  title: Text('Home'),
//              ),
//              BottomNavigationBarItem(
//                  icon: Icon(Icons.textsms),
//                  title: Text('Home'),
//              ),
//            ],
//          ),
        ),
      ),
      onWillPop: () {
        Navigator.of(context).pop(true);
        return null;
      },
    );
  }
}
