
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'nextpage.dart';

class HomeAppsRoute extends MaterialPageRoute{
  Map<String, dynamic> data;
  HomeAppsRoute({this.data}) : super(builder: (context) => new HomeApps(data: data));
}

class HomeApps extends StatelessWidget{
  final Map<String, dynamic> data;
  HomeApps({Key key,this.data}) : super(key: key);

  Widget tombolBuilder(BuildContext context,{IconData icon,String judul,int index }){
    return RaisedButton(
      onPressed: () {Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => NextPage(data: data, index: index,)));},
//                    textColor: Colors.white,
      padding: const EdgeInsets.all(0.0),
      child: Container(
        width: 170,
        height: 170,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.black),
            color: Colors.white
        ),
        child: Column(
          children: <Widget>[
            Icon(icon, size:130,),
//            Container(
//              width: 130,
//              height: 130,
//              decoration: BoxDecoration(
//                  color: Colors.white,
//                  shape: BoxShape.rectangle,
//                  image: DecorationImage(
//                    fit: BoxFit.cover,
//                    image: NetworkImage(gambar),
//                  )
//              ),
//            ),
            Text(judul)
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context){
//      return WillPopScope(
//          child: Scaffold(
    return Scaffold(
      backgroundColor: Color.fromARGB(255 , 255, 255, 255),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(125.0),
        child: AppBar(
//              automaticallyImplyLeading: false,
          title: Text('Chrono Scheduler'),
          flexibleSpace: Container(
            padding: const EdgeInsets.all(15.0),
            child: Column(
                children: <Widget>[
                  SizedBox(width: 5, height: 70,),
                  TextField(
                    style: TextStyle(fontSize: 15.0,),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(8.0),
                      filled: true,
                      fillColor: Colors.white,
                      border: const OutlineInputBorder(),
                    ),
                  ),
                ]
            ),
          ),
          backgroundColor: Color.fromARGB(255, 64, 140, 255),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage('https://via.placeholder.com/100'),
                      )
                  ),
                ),
                SizedBox(width: 20, height: 0,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text('Welcome,\n'+data["name"], style: TextStyle(fontSize: 20.0),),
//                    SizedBox(width: 1.0, height: 5.0,),
                  ],
                ),
              ],),
            SizedBox(height: 30, width: 10),
            Flexible(
              child: GridView.count(
                primary: false,
                padding: const EdgeInsets.all(0),
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                crossAxisCount: 2,
                children: <Widget>[
                  tombolBuilder(context,icon: Icons.calendar_today, judul: "Metting", index: 0),
                  tombolBuilder(context,icon: Icons.note, judul: "Notes", index: 1),
                  tombolBuilder(context,icon: Icons.notifications_active, judul: "Reminder", index: 3),
                  tombolBuilder(context,icon: Icons.settings, judul: "Settings", index: 4),
//                  Container(
//                    padding: const EdgeInsets.all(8),
//                    child: const Text('Sound of screams but the'),
//                    color: Colors.teal[300],
//                  ),
//                  Container(
//                    padding: const EdgeInsets.all(8),
//                    child: const Text('Who scream'),
//                    color: Colors.teal[400],
//                  ),
//                  Container(
//                    padding: const EdgeInsets.all(8),
//                    child: const Text('Revolution is coming...'),
//                    color: Colors.teal[500],
//                  ),
//                  Container(
//                    padding: const EdgeInsets.all(8),
//                    child: const Text('Revolution, they...'),
//                    color: Colors.teal[600],
//                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
//      onWillPop: () {
//        Navigator.of(context).pop(true);
//        return null;
//      },
//    );
  }
}